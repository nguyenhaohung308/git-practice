import math
class Shapes(object):
    ''' Calculate area, perimeter, width, length of rectangle/square
    Output: the area, perimeter, width, length of rectangle, square and the area of circle'''

    def calculate_width_of_square_from_perimeter(perimeter):
        width = perimeter/4
        return width
    
    def calculate_width_of_square_from_area(area):
        width = math.sqrt(area)
        return width

    def perimeter_of_square(Side):
        return Side * 4    
    
    def area_of_square(side):
        return side*side

    ''' Test data'''
